class User < ActiveRecord::Base
  has_secure_password
  
  validates :password, length: { minimum: 5}
  validates :email, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  
  def deliver_password_reset_instructions
    save_token
      
    PasswordResetNotifier.password_reset_instructions(self).deliver_now
  end
  
  def save_token
    self.perishable_token = SecureRandom.hex(4)
    save(validate: false)    
  end  
end
