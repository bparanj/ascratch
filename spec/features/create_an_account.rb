require 'rails_helper'
require 'spec_helper'

feature 'Account Management' do  
  scenario 'A user creates an account' do
    visit new_user_path
    
    fill_in "Email", with: 'bugs@rubyplus.com'
    fill_in "Password", with: '12345'
    
    click_button 'Signup'
    
    expect(page).to have_content('Logged in as bugs@rubyplus.com.')
  end

end